var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

var index = require('./routes/index');
var users = require('./routes/users');

var app = express();
const Instagram = require('node-instagram').default;
//https://api.instagram.com/oauth/authorize/?client_id=30314d4643ae49db9cfb6e06dba19298&redirect_uri=http://localhost:3000&response_type=code&scope=basic+public_content+follower_list+comments+relationships+likes
//code=f6ec5a15989d48aa9d084f92e4e96a29
//token=214598091.30314d4.0a7d4221ac88476c9f16ba317e4a9e00
// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', index);
app.use('/users', users);

// Create a new instance.
const instagram = new Instagram({
  clientId: 'your-client-id',
  accessToken: 'user-access-token',
});



// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
