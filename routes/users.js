var express = require('express');
var router = express.Router();
const Instagram = require('node-instagram').default;

// Create a new instance.
const instagram = new Instagram({
    clientId: '30314d4643ae49db9cfb6e06dba19298',
    accessToken: '214598091.30314d4.0a7d4221ac88476c9f16ba317e4a9e00'
});

/* GET users listing. */
router.get('/', function (req, res, next) {
    res.send('respond with a resource');
});

router.get('/self', function (req, res) {
    instagram.get('users/self', function (err, data) {
        res.render('index',
        {title : data.data.username});
    })
});

router.get('/recent', function (req, res) {
    instagram.get('users/self/media/recent', function (err, data) {
        res.render('index',
            {title : data.data[0].type});
        console.log(data.data[0]);
    })
});

router.get('/tag/:tag', function (req, res) {
    instagram.get('tags/'+req.params.tag , function (err, data) {
        //res.render('index',
        //    {title : data.data[0].type});
        console.log(data);
    })
});

router.get('/searchUser/:name', function (req, res) {
    instagram.get('users/search', { q: req.params.name }, function (err, data) {
        res.render('index',
            {title : data.data[0].username,
                imgSource : data.data[0].profile_picture});
        console.log(data);
    })
});



router.get('/tagSearch/:tagName', function (req, res) {
    instagram.get('tags/'+req.params.tagName+'/media/recent', function (err, data) {
        res.render('index',
            {title : data.data[0].username,
                imgSource : data.data[0].images.standard_resolution.url,
                data : data.data});
        console.log(data.data);
    })
});

module.exports = router;
